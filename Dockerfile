FROM python:3.5-alpine3.8
RUN apk add --update alpine-sdk libffi-dev build-base py3-pip python3-dev libressl-dev
ADD requirements.txt /requirements.txt
RUN pip install -r requirements.txt
ADD app.py /app.py
ADD dashboard.json /dashboard.json

CMD ["python","-u", "/app.py"]
EXPOSE 8989