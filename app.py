import asyncio

from aiohttp import web

import socketio
import json
import threading


sio = socketio.AsyncServer(async_mode='aiohttp')
app = web.Application()
sio.attach(app)

clients = list()

# preload file
with open('dashboard.json', 'r') as f:
    metricsData = json.load(f)

# e = threading.Event()
# while not e.wait(5):
#     sio.emit('metrics', json.dumps(metricsData))


async def background_task():
    """Example of how to send server generated events to clients."""
    while True:
        await sio.sleep(5)
        await sio.emit('metrics', json.dumps(metricsData))


@sio.on('connect')
async def test_connect(sid, environ):
    print("Socket connected")
    clients.append(sid)


@sio.on('metrics')
async def get_metrics(sid, environ):
    print("emit data")
    await sio.emit('metrics', json.dumps(metricsData))


@sio.on('disconnect')
def test_disconnect(sid):
    print('Client disconnected')


async def index(request):
    return web.json_response(metricsData)

app.router.add_get('/', index)


if __name__ == '__main__':
    sio.start_background_task(background_task)
    web.run_app(app)
